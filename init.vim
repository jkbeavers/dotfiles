set runtimepath^=~/.vim runtimepath+=~/.vimafter
let &packpath = &runtimepath

" No Plugins -- minmal mappings
source ~/.vimrc 

" Python3
let g:python3_host_prog="/usr/bin/python"

" Overriden to load both
nnoremap <leader>S :so ~/.vimrc<CR>:so ~/.config/nvim/init.vim<CR>

" Yank to clipboard
set clipboard+=unnamedplus

" Specify a directory for plugins 
call plug#begin('~/.vim/plugged')
" Fuzzy Finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Autocompletion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Git
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'

" ~~~~~~~~~~~~~~~~~~~~~ HI! Adding a new plugin? ~~~~~~~~~~~~~~~~~~~~~ 
" 1. Add the external dependency in "External Installs" 
" 2. Create a separate configuration file as ~/dotfiles/vimpluginsrc/*.vimrc
" 3. Source the *.vimrc under "Plugin Configurations" below
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

call plug#end()
" [ "External Installs" ]
" rg: ripgrep - searching real fast
" ccls: ccls - C/C++ lang server
" bat: bat - used for fzf preview window highlighting
let externals = [
    \'rg',
    \'ccls',
    \'bat',
    \]
for external in externals
    if !executable(external)
        echom "WARNING:" external "is not installed!"
    endif
endfor

" [ "Plugin Configurations" ]
source $HOME/dotfiles/vimpluginsrc/fzf-rg.vimrc
source $HOME/dotfiles/vimpluginsrc/coc.vimrc

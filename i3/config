# mod
set $mod Mod1

# font
font pango:fontawesome-webfont 10 
# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
#bindsym $mod+Return exec i3-sensible-terminal
bindsym $mod+Return exec urxvt
#create small floating window
bindsym $mod+Shift+Return exec urxvt; exec "bash -c 'sleep 0.2s;i3-msg floating enable;i3-msg resize set 500px 400px;i3-msg move position 50px 175px'"

# kill focused window
bindsym $mod+Shift+q kill

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# split in horizontal orientation
bindsym $mod+g split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
# bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

set $ws1 "1:"
set $ws2 "2:"
set $ws3 "3:"
set $ws4 "4:"
set $ws5 "5:"
set $ws6 "6:"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# Default Workspaces
# To find a window's class use xprop
assign [class="Chromium"] $ws3
assign [class="Firefox"] $ws4
assign [class="Thunderbird"] $ws5
# cause spotify is retarded
for_window [class="Spotify"] move to workspace $ws6

# Default monitor for workspaces
set $def eDP1
set $ext HDMI2
workspace $ws1 output $def
workspace $ws2 output $ext
workspace $ws3 output $ext
workspace $ws6 output $def

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Setting color from .Xresources
# takes color from .Xresources and sets it to color_var
# 					color_var			color		backup
set_from_resource $bg-color 			color0		#2f343f
set_from_resource $inactive-bg-color 	color8		#2f343f
set_from_resource $text-color 			color7		#f3f4f5
set_from_resource $inactive-text-color 	color15		#676E7D
set_from_resource $urgent-bg-color 		color9		#E53935


# window colors
#                       border              background         text                 indicator
client.focused          $bg-color         	$bg-color          $text-color          #00ff00
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          #00ff00

# borders such and such
hide_edge_borders both
new_window pixel 2

#--------------------- Applications ---------------------
set $HOME /home/taro

#dbus for cron stuff
exec --no-startup-id $HOME/dotfiles/bin/dbus.sh
#notifications
exec --no-startup-id dunst -config $HOME/.dunstrc
# make shit see-through (-f fade transition)
exec --no-startup-id compton -f  
# desktop background
set $bg_cmd feh --bg-scale $HOME/Pictures/wallpapers/books.jpg
exec_always $bg_cmd
#--------------------- My Keybindings ---------------------

# Toggle movement
bindsym $mod+n workspace next
bindsym $mod+p workspace prev

# start rofi (dmenu) (a program launcher)
bindsym $mod+d exec rofi -show run

# Set Volume
bindsym XF86AudioRaiseVolume exec --no-startup-id ~/dotfiles/bin/changeVolume 1 # increase
bindsym XF86AudioLowerVolume exec --no-startup-id ~/dotfiles/bin/changeVolume 0 # decrease
bindsym XF86AudioMute exec --no-startup-id ~/dotfiles/bin/changeVolume 2 # mute
# Set Brightness (Fn + Home/End)
bindsym XF86MonBrightnessUp exec --no-startup-id ~/dotfiles/bin/changeBrightness 1 # increase 
bindsym XF86MonBrightnessDown exec --no-startup-id ~/dotfiles/bin/changeBrightness 0 # decrease
# Show Battery 
bindsym XF86LaunchA exec --no-startup-id ~/dotfiles/bin/battery.sh

# Spotify keys
bindsym F10 exec "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"
bindsym F11 exec "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"
bindsym F12 exec "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"

exec_always --no-startup-id $HOME/.config/polybar/launch.sh

#--------------------- Modes ----------------------
# Manage System Stuff (shutdown, etc.)
bindsym $mod+c mode "  "

mode "  " {
		# Start polybar cause its retarded
		bindsym p exec --no-startup-id $HOME/.polybar/launch.sh; mode "default"
		# Shutdown
		bindsym s exec --no-startup-id systemctl poweroff 
		# Restart
		bindsym r exec --no-startup-id systemctl reboot
		#Suspend
		bindsym h exec --no-startup-id systemctl suspend; mode "default"
		# Lock screen son
		bindsym l exec --no-startup-id betterlockscreen -l; mode "default"
		# exit i3 (logs you out of your X session)
		bindsym e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

		# Return to normal mode: Enter or Escape
		bindsym Return mode "default"
		bindsym Escape mode "default"
}

# Display Mode (adding/removing HDMI)
bindsym $mod+x mode "  "

mode "  " {
	bindsym l exec --no-startup-id xrandr --output $ext --auto --left-of $def, mode "default"; exec --no-startup-id $bg_cmd
	bindsym r exec --no-startup-id xrandr --output $ext --auto --right-of $def, mode "default"; exec --no-startup-id $bg_cmd
	bindsym o exec --no-startup-id xrandr --output $ext --off, mode "default"

	# Return to normal mode: Enter or Escape
	bindsym Return mode "default"
	bindsym Escape mode "default"
}
# Resize Windows
bindsym $mod+r mode "  "
mode "  " {
        bindsym h resize shrink frame_width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow frame_width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

" Search filenames
noremap <leader>sf :Files<CR>
noremap <leader>sa :Rg<CR>
noremap <leader>sl :Lines<CR>
nnoremap <silent> <Leader><Enter> :call <sid>cleanEmptyBuffers()<CR> :Buffers<CR>
nnoremap <C-]> :FZFTags<CR>

let $FZF_DEFAULT_COMMAND='rg --files --ignore-file /builds/dotfiles/rg-ignore'

let g:fzf_layout = { 'down': '~30%' }
let g:fzf_buffers_jump = 1
let g:fzf_tags_command = 'ctags -R --exclude=build* --exclude=docs'

if executable('rg')
    set grepprg=rg\ --no-heading\ --vimgrep "!.git/*"
    set grepformat=%f:%l:%c:%m
endif

command! -bang -nargs=? -complete=dir Files
  \ call fzf#vim#files(<q-args>, {'options': ['--layout=reverse', '--info=inline']}, <bang>0)

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview(),
  \   <bang>0)

command! -bang -nargs=? -complete=dir Buffers
  \ call fzf#vim#buffers(<q-args>, {'options': ['--layout=reverse', '--info=inline']}, <bang>0)

" [[B]Commits] Customize the options used by 'git log':
let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'

function! s:cleanEmptyBuffers()
    let buffers = filter(range(1, bufnr('$')), 'buflisted(v:val) && empty(bufname(v:val)) && bufwinnr(v:val)<0 && !getbufvar(v:val, "&mod")')
    if !empty(buffers)
        exe 'bw ' . join(buffers, ' ')
    endif
endfunction

PS1="\[\033[1;37m\]\u\[\033[0m\] \[\033[0;35m\]>>\[\033[0m\] "
#export LC_ALL=
#export LC_COLLATE="C"
alias python=/usr/bin/python3
alias gs="git status"
alias glog="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
export PATH=$PATH:~/dotfiles/bin
export VISUAL="vim"
export WORKON_HOME=$HOME/.virtualenvs
export MSYS_HOME=/c/msys/1.0
source /usr/local/bin/virtualenvwrapper.sh

set -o vi
# Auto-start i3
if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
	exec startx
fi

if [[ -z "$TMUX" ]] 
then
	ID="`tmux ls | grep -vm1 attached | cut -d: -f1`" # get the id of a deattached session
	if [[ -z "$ID" ]] ;then # if not available create a new one
		tmux new-session
	else
		tmux attach-session -t "$ID" # if available attach to it
	fi
fi

alias bat='acpi -b | grep Battery'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

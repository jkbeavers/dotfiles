" No Plugins -- minmal mappings

autocmd FileType * setlocal shiftwidth=4 softtabstop=4 expandtab

au BufNewFile,BufRead *.py
    \ set expandtab       " replace tabs with spaces
    \ set autoindent      " copy indent when starting a new line
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4

" Make backspace behave in a sane manner.
set backspace=indent,eol,start

" Switch syntax highlighting on
syntax on

" get rid of vi compatibility
set nocompatible

" I dunno what modelines are. yet... this is a security thing for now
set modelines=0

" Show status line
set laststatus=2

" set to autoread when a file is changed outside
set autoread


" Good search
set smartcase
set ignorecase
set incsearch
set gdefault
set showmatch
set hlsearch " maybe stop searching via :nohlearch ?

" be smart with yo tabs
set smarttab

" Show current line + relative around it
set relativenumber
set number

" Allow hidden buffers, don't limit to 1 file per window/split
set hidden

" Increase number of commands held in history
set history=100

" Faster drawing
set ttyfast

" handle long lines well
set wrap
set textwidth=80
set colorcolumn=80

" Control auto formatting
set formatoptions=tc " wrap text and comments using textwidth
set formatoptions+=r " continue comments with ENTER in <insert> mode
set formatoptions+=q " enable formatting of comments with gq
set formatoptions+=n " detect lists and format
set formatoptions+=b " auto-wrap in insert mode, but not old long lines

let mapleader = " "

" Show whitespace
:set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
nnoremap <leader>w :set list!<CR>

" new tab
nnoremap <leader>t :tabnew<CR>

" switch tab forward
nnoremap <leader>n :tabnext<CR>

" switch tab back
nnoremap <leader>N :tabprev<CR>

" clear results from search
nnoremap <leader><space> :noh<cr>

" easily view buffers
nnoremap <leader>b :buffers<cr>

" quickly access vimrc in a new tab
nnoremap <leader>E :tabnew<CR>:e $MYVIMRC<cr>

" open new vsplit and switch over to it
nnoremap <leader>v <C-w>v<C-w>l

" navigate splits using leader
nnoremap <leader>l <C-w>l
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>h <C-w>h

" Maximize pane for vsplit
" nnoremap <leader>z :vertical resize<CR>
" " Normal Size for vsplit
" nnoremap <leader>Z <C-w>=

" source vimrc
nnoremap <leader>SS :so ~/.vimrc<CR>

" Open todo file
nnoremap <leader>o :tabnew<CR>:e ~/todo.md<CR>'T

" Close buffer
nnoremap <leader>c :bd %<CR>

" Enable file type detection and do language-dependent indenting.
filetype plugin indent on

" Custom External Commands
" command Tex execute "w" | execute "silent !pdflatex %" | execute "redraw!"
" command TexErr execute "w" | execute "!pdflatex %" 

" Pass yanked text to netcat to transfer to clipboard
" This is server
" Client is expected to
"  - have a ssh tunnel from this localhost:2000 to the " client's localhost:2000
"  - have active process listening: nc -l 2000 | xclip
"      - Will nc -l will exit, so just do while true; do ...; done
" Can't figure out how to yank and do this in the same command with a selection
"function SendYankedText()
"    :call system("nc localhost 2001", getreg("@0"))
"endfunction

"nnoremap <leader>y :call SendYankedText()<CR>

if &diff
    colorscheme diffcolors
endif
